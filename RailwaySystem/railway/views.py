from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User, auth

# Create your views here.


def index(request):
    message = "Hello Priyanka!"
    return render(request, 'railway/index.html', {'message': message})


def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return HttpResponseRedirect('/railway/index')
        else:
            return HttpResponseRedirect('railway/login')
    else:
        return render(request, 'railway/login.html')


def register(request):
    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']

        user = User.objects.create_user(
            username=username, password=password1, first_name=first_name, last_name=last_name, email=email)
        user.save()
        print("User created successfully!")
        return HttpResponseRedirect('/railway/index')

    else:
        return render(request, 'railway/register.html')


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/railway/index')
