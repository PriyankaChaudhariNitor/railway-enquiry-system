from django.db import models

# Create your models here.


class Train(models.Model):

    def __str__(self):
        return self.train_name

    train_number = models.CharField(primary_key=True, max_length=100)
    train_name = models.CharField(max_length=2000)
    route = models.ForeignKey(
        'Route', on_delete=models.CASCADE)

    class Meta:
        db_table = "trains"


class Route(models.Model):
    def __str__(self):
        return self.source_station + "--"+self.dest_station
        # return str(self.id)

    # route_id = models.CharField(max_length=50, unique=True)
    source_station = models.CharField(max_length=300)
    dest_station = models.CharField(max_length=300)

    class Meta:
        db_table = "routes"


class Station(models.Model):
    def __str__(self):
        return self.station_name

    station_id = models.AutoField(primary_key=True)
    station_name = models.CharField(max_length=50)

    class Meta:
        db_table = "stations"


class RouteStation(models.Model):
    def __str__(self):
        return str(self.train_number) + str(self.station_id) + str(self.route_id) + str(self.atime)

    train_number = models.ForeignKey('Train', on_delete=models.CASCADE)
    station_id = models.ForeignKey('Station', on_delete=models.CASCADE)
    route_id = models.ForeignKey('Route', on_delete=models.CASCADE)
    # order = models.IntegerField()
    atime = models.TimeField()

    class Meta:
        db_table = "route_station"
