from django.urls import path
from . import views

app_name = 'train'
urlpatterns = [
    path('get-train/', views.getTrain, name="getTrain"),
    path('add-train/', views.createTrain, name="createTrain"),
    path('update-train/<str:id>/', views.updateTrain, name="updateTrain"),
    path('delete-train/<str:id>/', views.deleteTrain, name="deleteTrain"),

    #### route ####
    path('get-route/', views.getRoute, name="getRoute"),
    path('add-route/', views.createRoute, name="createRoute"),
    path('update-route/<int:id>/', views.updateRoute, name="updateRoute"),
    path('delete-route/<int:id>/', views.deleteRoute, name="deleteRoute"),

    #### station ####
    path('get-stations/', views.getStations, name="getStations"),
    path('add-station/', views.createStation, name="createStation"),
    path('update-station/<str:id>/', views.updateStation, name="updateStation"),
    path('delete-station/<str:id>/', views.deleteStation, name="deleteStation"),

    #### Route Station ####
    path('add-routestation/', views.createRouteStation, name="createRouteStation"),
    path('get-routestation/', views.getRouteStation, name="getRouteStation"),
    path('update-routestation/<int:id>/',
         views.updateRouteStation, name="updateRouteStation"),
    path('delete-routestation/<int:id>/',
         views.deleteRouteStatio, name="deleteRouteStatio"),
]
