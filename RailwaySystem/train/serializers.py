from .models import *
from rest_framework import fields, serializers


class TrainSerializer(serializers.ModelSerializer):
    class Meta:
        model = Train
        fields = "__all__"
