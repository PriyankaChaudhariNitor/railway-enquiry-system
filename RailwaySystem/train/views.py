from typing import final
from django import forms
from django.http import request
from .models import RouteStation, Station, Train, Route
from django.shortcuts import redirect, render
from .forms import TrainForm, RouteForm, StationForm, RouteStationForm


def getTrain(request):
    allTrain = Train.objects.all().values()
    routeData = Route.objects.all().values()

    final_data = []
    for train in allTrain:
        data = {}
        for route in routeData:
            if(route['id'] == train['route_id']):
                data['source_station'] = route['source_station']
                data['dest_station'] = route['dest_station']
                data['train_number'] = train['train_number']
                data['train_name'] = train['train_name']

        final_data.append(data)

    return render(request, "train/get_train.html", {'final_data': final_data})


def createTrain(request):
    if request.method == 'POST':
        form = TrainForm(request.POST or None)

        if form.is_valid():
            form.save()
        else:
            pass
        return redirect("/train/get-train")
    else:
        routeData = Route.objects.all().values()
        form = TrainForm()
        return render(request, "train/addTrain.html", {'routeData': routeData, 'form': form})


def updateTrain(request, id):
    train_detail = Train.objects.get(train_number=id)

    form = TrainForm(request.POST or None, instance=train_detail)

    if form.is_valid():
        form.save()
        return redirect("/train/get-train")

    return render(request, "train/addTrain.html", {'form': form, 'train_detail': train_detail})


def deleteTrain(request, id):
    train_detail = Train.objects.get(train_number=id)

    if request.method == 'POST':
        train_detail.delete()
        return redirect("/train/get-train")

    return render(request, 'train/delete_train.html', {'train_detail': train_detail})


####### Route ######
def getRoute(request):
    routeData = Route.objects.all().values()
    return render(request, "train/get_route.html", {'routeData': routeData})


def createRoute(request):
    if request.method == 'POST':
        form = RouteForm(request.POST or None)

        if form.is_valid():
            form.save()
        else:
            pass
        return redirect("/train/get-route")
    else:
        form = RouteForm()
        return render(request, "train/addRoute.html", {'form': form})


def updateRoute(request, id):
    route_detail = Route.objects.get(id=id)

    form = RouteForm(request.POST or None, instance=route_detail)

    if form.is_valid():
        form.save()
        return redirect("/train/get-route")

    return render(request, "train/addRoute.html", {'form': form, 'route_detail': route_detail})


def deleteRoute(request, id):
    route_detail = Route.objects.get(id=id)
    print("Hiiiii Priyanka", id, request.method)
    if request.method == 'POST':

        route_detail.delete()
        return redirect("/train/get-route")

    return render(request, 'train/get_route.html', {'route_detail': route_detail})


######################### Station ##############################
def getStations(request):
    stationData = Station.objects.all().values()
    return render(request, "train/get_stations.html", {'stationData': stationData})


def createStation(request):
    if request.method == 'POST':
        form = StationForm(request.POST or None)

        if form.is_valid():
            form.save()
        else:
            pass
        return redirect("/train/get-stations")
    else:
        form = StationForm()
        return render(request, "train/addStation.html", {'form': form})


def updateStation(request, id):
    station_detail = Station.objects.get(pk=id)

    form = StationForm(request.POST or None, instance=station_detail)

    if form.is_valid():
        form.save()
        return redirect("/train/get-stations")

    return render(request, "train/addStation.html", {'form': form, 'station_detail': station_detail})


def deleteStation(request, id):
    station_detail = Station.objects.get(pk=id)

    if request.method == 'POST':
        station_detail.delete()
        return redirect("/train/get-stations")

    return render(request, 'train/delete_station.html', {'station_detail': station_detail})


#### Route stations ####
def getRouteStation(request):
    routestations = RouteStation.objects.all()
    # print(stationData)
    return render(request, "train/get_routestation.html", {'routestations': routestations})


def createRouteStation(request):
    if request.method == 'POST':
        form = RouteStationForm(request.POST or None)

        if form.is_valid():
            form.save()
        else:
            pass
        return redirect("/train/get-routestation")
    else:
        form = RouteStationForm()
        return render(request, "train/addRouteStation.html", {'form': form})


def updateRouteStation(request, id):
    routestation_detail = RouteStation.objects.get(pk=id)

    form = RouteStationForm(request.POST or None, instance=routestation_detail)

    if form.is_valid():
        form.save()
        return redirect("/train/get-routestation")

    return render(request, "train/addRouteStation.html", {'form': form, 'routestation_detail': routestation_detail})


def deleteRouteStatio(request, id):
    routestation_detail = RouteStation.objects.get(pk=id)

    if request.method == 'POST':
        routestation_detail.delete()
        return redirect("/train/get-routestation")

    return render(request, 'train/get_routestation.html', {'routestation_detail': routestation_detail})
