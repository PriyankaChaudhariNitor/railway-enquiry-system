from django import forms
from django.db import models
from django.forms import fields
from .models import Station, Train, Route, RouteStation


class TrainForm(forms.ModelForm):
    class Meta:
        model = Train
        fields = ['train_number', 'train_name', 'route']


class RouteForm(forms.ModelForm):
    class Meta:
        model = Route
        fields = ['source_station', 'dest_station']


class StationForm(forms.ModelForm):
    class Meta:
        model = Station
        fields = ['station_name']


class RouteStationForm(forms.ModelForm):
    class Meta:
        model = RouteStation
        fields = ['train_number', 'station_id', 'route_id', 'atime']
